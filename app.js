//nunca subir la carpeta node_modules a los repositorios
//basta con el package.json y ejecutar npm install para recontruir los modulos

const colors = require('colors');
// const { alias } = require('yargs');
const argv = require('./config/yargs').argv;
const {
    crearArchivo,
    listarTabla
} = require('./multiplicador/multiplicar');

let comando = argv._[0];

switch (comando) {
    case 'listar':
        listarTabla(argv.base, argv.limite)
            .then(tabla => console.log(`Tabla: ${tabla}`))
            .catch(err => console.log(err))
        break;
    case 'crear':
        crearArchivo(argv.base, argv.limite)
            .then(archivo => console.log('Archivo creado :', `${archivo}`.cyan))
            .catch(err => console.log(err))
        break;
    default:
        console.log('Comando no reconocido');
}

// console.log(process.argv);

// let argv2 = process.argv;
// console.log('limite',argv.limite);

// let param = argv[2];

// let base = param.split('=')[1];
// console.log(base);