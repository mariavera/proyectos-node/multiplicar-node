//requireds
const fs = require('fs'); //libreria propia de node
const colors = require('colors');

// const fs = require('express'); // paquete externo que se instala y luego se usa
// const fs = require('./path'); //nuestro archivos

let listarTabla = (base, limite) => {
    return new Promise((resolve, reject) => {


        if( !Number(base) || !Number(limite)){
            reject('No es un numero');
            return;
        }
        let data = '';
        console.log('====================');
        console.log('=======TABLA======'.magenta);
        console.log('====================');
        for (let i = 1; i <= limite; i++) {
            a = base * i;
            data += `${base} * ${i} = ${a}\n`;
            
        }
        resolve(data)
        


})
}
let crearArchivo = (base,limite = 10) => {
    return new Promise((resolve, reject) => {

        if( !Number(base)){
            reject('No es un numero');
            return;
        }
        let data = '';
        for (let i = 1; i <= limite; i++) {
            a = base * i;
            data += `${base} * ${i} = ${a}\n`;
        }

        // const data = new Uint8Array(Buffer.from('Hello Node.js'));
        fs.writeFile(`tablas/tabla-${base}.txt`, data, (err) => {
            if (err) reject(err)
            else
            resolve(`tabla-${base}.txt`)
            console.log(`El archivo  ha sido creado`);
        });
    });
}

module.exports ={
    crearArchivo,
    listarTabla
}