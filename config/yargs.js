const opts = {
    base: {
        demandOption: true,
        alias: 'b'
    },
    limite: {
        alias: 'l',
        default: 10

    }

}
const argv = require('yargs')
    .command('listar', 'imprimir en consola la tabla de multiplicar', opts)
    .command('crear', 'crear un archivo con la tabla definida', opts)
    .help()
    .argv;

module.exports = {
    argv
}